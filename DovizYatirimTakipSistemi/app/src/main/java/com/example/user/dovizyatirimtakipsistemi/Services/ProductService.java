package com.example.user.dovizyatirimtakipsistemi.Services;

import com.example.user.dovizyatirimtakipsistemi.Entities.Responses;
import com.example.user.dovizyatirimtakipsistemi.Entities.ValueItem;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by user on 4.11.2018.
 */

public interface ProductService {

    @GET("rates")  //KUVEYTTURK TUM DOVIZ KURLARI
    Call<Responses> findAllRates();
}
