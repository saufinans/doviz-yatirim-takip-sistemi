package com.example.user.dovizyatirimtakipsistemi.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.dovizyatirimtakipsistemi.Entities.ValueItem;
import com.example.user.dovizyatirimtakipsistemi.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by user on 4.11.2018.
 */

public class DovizKurlariAdapter extends BaseAdapter {


    List<ValueItem> list;
    Context context;
    Activity activity;

    public DovizKurlariAdapter(List<ValueItem> list, Context context , Activity actvity) {

        this.list = list;
        this.context = context;
        this.activity=actvity;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view= LayoutInflater.from(context).inflate(R.layout.foradapter_kurlar,viewGroup,false);
        LinearLayout kompleLayout  = view.findViewById(R.id.KompleLayout);
        TextView tvKurAdi=(TextView)view.findViewById(R.id.textViewKurlarKurAdi);
        TextView tvAlis=(TextView)view.findViewById(R.id.textViewKurlarAlis);
        TextView tvSatis=(TextView)view.findViewById(R.id.textViewKurlarSatis);
        Button btnAl=view.findViewById(R.id.buttonKurlarAl);
        Button btnSat=view.findViewById(R.id.buttonKurlarSat);
//BUTTONLARA CLICK VERILIP SAYFA YONLENDIRMELERI YAPILACAK
        tvKurAdi.setText(""+list.get(i).getFxCode());
        tvAlis.setText( ""+list.get(i).getBuyRate());
        tvSatis.setText(""+list.get(i).getSellRate());


        //KOMPLE LAYOUTA CLICK VERILECEK
       /*
        final String post = ""+list.get(i).getID();
        final String resim = ""+list.get(i).getName();
        final double price = list.get(i).getPrice();
        final double quantity = list.get(i).getQuantity();
        kompleLayout.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
          Intent intent = new Intent(activity, Activity2.class);
          intent.putExtra("post_id",post);
          activity.startActivity(intent);
          }
          }
          );*/

        return view;
    }



}
