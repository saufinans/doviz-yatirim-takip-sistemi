package com.example.user.dovizyatirimtakipsistemi.Entities;

import java.util.List;

public class Responses{
      private boolean success;
    private List<ValueItem> value;
    private List<Object> results;

    public void setSuccess(boolean success){
        this.success = success;
    }

    public boolean isSuccess(){
        return success;
    }

    public void setValue(List<ValueItem> value){
        this.value = value;
    }

    public List<ValueItem> getValue(){
        return value;
    }

    public void setResults(List<Object> results){
        this.results = results;
    }

    public List<Object> getResults(){
        return results;
    }

    @Override
    public String toString(){
        return
                "Response{" +
                        "success = '" + success + '\'' +
                        ",value = '" + value + '\'' +
                        ",results = '" + results + '\'' +
                        "}";
    }
}