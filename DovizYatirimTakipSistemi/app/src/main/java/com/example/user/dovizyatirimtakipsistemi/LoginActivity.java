package com.example.user.dovizyatirimtakipsistemi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {


    EditText et_userName,et_password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Tanimla();
    }

    public void Tanimla()
    {
        et_userName= (EditText) findViewById(R.id.editTextUsername);
        et_password= (EditText) findViewById(R.id.editTextPassword);

    }


    public void GirisYap(View v)
    {
        String ka = et_userName.getText().toString();
        String ps = et_password.getText().toString();
        if(ka.equals("admin") && ps.equals("admin"))
        {
            Intent intent = new Intent(LoginActivity.this , MainActivity.class);
            startActivity(intent);


        }

        else
        {

            Toast.makeText(this, "Hatalı Kullanıcı Adı Veya Şifre", Toast.LENGTH_LONG).show();

        }


    }
}
