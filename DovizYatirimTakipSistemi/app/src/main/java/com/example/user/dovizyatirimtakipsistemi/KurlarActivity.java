package com.example.user.dovizyatirimtakipsistemi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.dovizyatirimtakipsistemi.Adapters.DovizKurlariAdapter;
import com.example.user.dovizyatirimtakipsistemi.Entities.Responses;
import com.example.user.dovizyatirimtakipsistemi.Entities.ValueItem;
import com.example.user.dovizyatirimtakipsistemi.Services.APIClient;
import com.example.user.dovizyatirimtakipsistemi.Services.ProductService;
import com.example.user.dovizyatirimtakipsistemi.URLs.URLs;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KurlarActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    ListView liste;    //listViewvi aldık
    List<ValueItem> list;     //Kuveyt Apisinden  dönen Doviz Bilgilerini alıcaz
SwipeRefreshLayout swipper;
    TextView tx_LastRefreshDate;
    Date currentTime;
    String saniye;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kurlar);
        tanimla();
        kurBilgileriniGetir();




    }

    public void tanimla()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        liste = (ListView) findViewById(R.id.listViewKurlar);
        tx_LastRefreshDate= (TextView) findViewById(R.id.tx_LastRefreshDate);

        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        swipper= (SwipeRefreshLayout) findViewById(R.id.Swipe);

        swipper.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                list.clear();
                liste = null;
                liste = (ListView) findViewById(R.id.listViewKurlar);
                kurBilgileriniGetir();


           new Handler().postDelayed(new Runnable() {
               @Override
               public void run() {
                   swipper.setRefreshing(false);
               }
           },0);
            }
        });

    }

    public void kurBilgileriniGetir()
    {
        currentTime = Calendar.getInstance().getTime();
        saniye=""+currentTime.getSeconds();
        if(saniye.length()==1)
        {
            saniye="0"+saniye;
        }
        tx_LastRefreshDate.setText(""+currentTime.getHours()+":"+currentTime.getMinutes()+":"+saniye);

        list  = new ArrayList<>();

        //BEKLEME EKRANI YAPALIM
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Bilgi");
        progressDialog.setMessage("Yükleniyor");
        progressDialog.setCancelable(false);
        progressDialog.show();
        //YAPTIK

        URLs url=new URLs(); //Url sınıfından base url çekeceğiz

        ProductService productSevice = APIClient.getClient(url.getUrlKuveyt()).create(ProductService.class); //url clasımızdan kuveytinkini çektik
        Call<Responses> bilgiList = productSevice.findAllRates(); //Apiye istek attık bi liste donderdi oradan value alanını alıcaz doviz bilgileri orda
        bilgiList.enqueue(new Callback<Responses>() {
            @Override
            public void onResponse(Call<Responses> call, Response<Responses> response) {

                if(response.isSuccessful())
                {
                  list  = response.body().getValue();
                 DovizKurlariAdapter adp = new DovizKurlariAdapter(list,getApplicationContext(),KurlarActivity.this);
                    progressDialog.cancel();


                    if(adp == null)
                        Toast.makeText(KurlarActivity.this, "Data Bulunamadı", Toast.LENGTH_SHORT).show();
                    else
                    {
                        liste.setAdapter(adp);


                    }

                }



            }

            @Override
            public void onFailure(Call<Responses> call, Throwable t) {
                Toast.makeText(KurlarActivity.this, "Bağlantıda Hata oluştu", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_anasayfa) {
            Intent intent=new Intent(KurlarActivity.this,MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_yatirimlar) {
            Intent intent=new Intent(KurlarActivity.this,YatirimlarActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_kurlar) {
        //Kurlar Sayfasındayız zaten birşey yapma
        } else if (id == R.id.nav_hesaplar) {
            Intent intent=new Intent(KurlarActivity.this,HesaplarActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
