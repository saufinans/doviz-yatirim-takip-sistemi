package com.example.user.dovizyatirimtakipsistemi;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    LinearLayout AnaLayout;
    float alim;
    float satim;
    float karZarar;
    TextView tv_alim , tv_satim , tv_karzarar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Tanimla();


        karMiZararMi();
    }

    private void Tanimla() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        AnaLayout = (LinearLayout) findViewById(R.id.ly_AnaLayout);
        tv_alim = (TextView) findViewById(R.id.textViewAlim);
        tv_satim = (TextView) findViewById(R.id.textViewSatim);
        tv_karzarar = (TextView) findViewById(R.id.TextViewKarMiZararMi);


    }

    private void karMiZararMi() {
        alim = 640.00f;// APIDEN ALACAGIZ
        satim = 700.00f;// APIDEN ALACAGIZ
        karZarar = satim-alim;
        if(karZarar < 0f)
        {
            AnaLayout.setBackgroundColor(Integer.decode (String.valueOf(getResources().getColor(R.color.colorZarar))));
        }
        else if(karZarar > 0f)
        {
            AnaLayout.setBackgroundColor(Integer.decode (String.valueOf(getResources().getColor(R.color.colorKar))));
        }
        else
        {
            AnaLayout.setBackgroundColor(Integer.decode (String.valueOf(getResources().getColor(R.color.colorNeKarNeZarar))));
        }

        tv_alim.setText(alim+" TL");
        tv_satim.setText(satim+" TL");
        tv_karzarar.setText(karZarar+" TL");
       }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_anasayfa) {
        //Abasayfadayız zatem bieşey yapma
        } else if (id == R.id.nav_yatirimlar) {
            Intent intent=new Intent(MainActivity.this,YatirimlarActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_kurlar) {
            Intent intent=new Intent(MainActivity.this,KurlarActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_hesaplar) {
            Intent intent=new Intent(MainActivity.this,HesaplarActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
