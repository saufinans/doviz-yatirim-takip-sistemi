package com.example.user.dovizyatirimtakipsistemi.Entities;

public class ValueItem{
	private double sellRate;
	private double parityBuyRate;
	private String fxCode;
	private String name;
	private int fxId;
	private double paritySellRate;
	private double buyRate;

	public void setSellRate(double sellRate){
		this.sellRate = sellRate;
	}

	public double getSellRate(){
		return sellRate;
	}

	public void setParityBuyRate(double parityBuyRate){
		this.parityBuyRate = parityBuyRate;
	}

	public double getParityBuyRate(){
		return parityBuyRate;
	}

	public void setFxCode(String fxCode){
		this.fxCode = fxCode;
	}

	public String getFxCode(){
		return fxCode;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setFxId(int fxId){
		this.fxId = fxId;
	}

	public int getFxId(){
		return fxId;
	}

	public void setParitySellRate(double paritySellRate){
		this.paritySellRate = paritySellRate;
	}

	public double getParitySellRate(){
		return paritySellRate;
	}

	public void setBuyRate(double buyRate){
		this.buyRate = buyRate;
	}

	public double getBuyRate(){
		return buyRate;
	}

	@Override
 	public String toString(){
		return 
			"ValueItem{" + 
			"sellRate = '" + sellRate + '\'' + 
			",parityBuyRate = '" + parityBuyRate + '\'' + 
			",fxCode = '" + fxCode + '\'' + 
			",name = '" + name + '\'' + 
			",fxId = '" + fxId + '\'' + 
			",paritySellRate = '" + paritySellRate + '\'' + 
			",buyRate = '" + buyRate + '\'' + 
			"}";
		}
}
