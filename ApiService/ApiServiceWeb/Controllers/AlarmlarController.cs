﻿using ApiServiceDAL;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ApiServiceWeb.Controllers
{
    [RoutePrefix("api/Alarmlar")]
    public class AlarmlarController : ApiController
    {
        AlarmlarDAL dal = new AlarmlarDAL();

        [ResponseType(typeof(IEnumerable<Alarmlar>))]
        [HttpGet]
        [Route("findall")]
        public IHttpActionResult GetAlarmlar()
        {
            try
            {
                var datam = dal.GetAllAlarms();
                return Ok(datam);
            }
            catch (System.Exception)
            {

                return BadRequest();
            }

        }

        [ResponseType(typeof(Alarmlar))]
        [HttpGet]
        [Route("find/{id}")]
        public IHttpActionResult GetAlarmlarByID(int id)
        {
            try
            {
                var datam = dal.GetAllAlarmsById(id);
                if (datam == null)
                    return NotFound();
                return Ok(datam);
            }
            catch (System.Exception)
            {

                return BadRequest();
            }

        }

        [ResponseType(typeof(Alarmlar))]
        [HttpPost]
        [Route("create")]
        public IHttpActionResult PostAlarmlar(Alarmlar alarmlar)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var datam = dal.CreateAlarms(alarmlar);

                    return CreatedAtRoute("DefaultApi", new { id = alarmlar.SQ_Id }, datam);
                }
                return BadRequest(ModelState);
            }
            catch (System.Exception)
            {

                return BadRequest();
            }

        }

        [ResponseType(typeof(Alarmlar))]
        [HttpPut]
        [Route("update")]
        public IHttpActionResult PutAlarmlar(int id, Alarmlar alarmlar)
        {
            try
            {
                if (dal.IsThereAnyData(id) == false)
                {
                    return NotFound();
                }
                else if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                else
                {
                    var datam = dal.UpdateAlarms(id, alarmlar);
                    return Ok(datam);
                }
            }
            catch (System.Exception)
            {

                return BadRequest();
            }

        }
         
        [HttpDelete]
        [Route("delete")]
        public IHttpActionResult DeleteAlarmlar(int id)
        {
            try
            {
                if (dal.IsThereAnyData(id) == false)
                {
                    return NotFound();
                }
                else
                {
                    dal.DeleteAlarms(id);
                    return Ok();
                }
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }
    }
}
