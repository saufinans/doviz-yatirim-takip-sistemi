﻿using ApiServiceDAL;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ApiServiceWeb.Controllers
{
    [RoutePrefix("api/HesapTipi")]
    public class HesapTipController : ApiController
    {
        HesapTipDAL dal = new HesapTipDAL();

        [ResponseType(typeof(IEnumerable<HesapTipleri>))]
        [HttpGet]
        [Route("findall")]
        public IHttpActionResult GetHesapTips()
        {
            try
            {
                var datam = dal.GetHesapTips();
                return Ok(datam);
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }

        [ResponseType(typeof(HesapTipleri))]
        [HttpGet]
        [Route("find/{id}")]
        public IHttpActionResult GetHesapTipsByID(int id)
        {
            try
            {
                var datam = dal.GetHesapTipsByID(id);
                if (datam == null)
                {
                    return NotFound();
                }

                return Ok(datam);
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }

        [ResponseType(typeof(HesapTipleri))]
        [HttpPost]
        [Route("create")]
        public IHttpActionResult PostHesapTips(HesapTipleri hesapTipleri)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var datam = dal.CreateHesapTips(hesapTipleri);

                    return CreatedAtRoute("DefaultApi", new { id = hesapTipleri.SQ_Id }, datam);
                }
                return BadRequest(ModelState);
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }

        [ResponseType(typeof(HesapTipleri))]
        [HttpPut]
        [Route("update")]
        public IHttpActionResult PutHesapTips(int id, HesapTipleri hesapTipleri)
        {
            try
            {
                if (dal.IsThereAnyData(id) == false)
                {
                    return NotFound();
                }
                else if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                else
                {
                    var datam = dal.UpdateHesapTipleris(id, hesapTipleri);
                    return Ok(hesapTipleri);
                }
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }
         
        [HttpDelete]
        [Route("delete")]
        public IHttpActionResult DeleteHesapTipleri(int id)
        {
            try
            {
                if (dal.IsThereAnyData(id) == false)
                {
                    return NotFound();
                }
                else
                {
                    dal.DeleteHesapTipleris(id);
                    return Ok();
                }
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }
    }
}
