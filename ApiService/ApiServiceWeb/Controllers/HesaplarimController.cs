﻿using ApiServiceDAL;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;


namespace ApiServiceWeb.Controllers
{
    [RoutePrefix("api/Hesaplarim")]
    public class HesaplarimController : ApiController
    {

        HesaplarDAL dal = new HesaplarDAL();

        [ResponseType(typeof(IEnumerable<Hesaplarim>))]
        [HttpGet]
        [Route("findall")]
        public IHttpActionResult GetAllHesaplarim()
        {
            try
            {
                var datam = dal.GetAllHesaplar();
                return Ok(datam);
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }

        [ResponseType(typeof(Hesaplarim))]
        [HttpGet]
        [Route("find/{id}")]
        public IHttpActionResult GetHesaplarimById(int id)
        {
            try
            {
                var datam = dal.GetAllHesapsById(id);
                if (datam == null)
                {
                    return NotFound();
                }

                return Ok(datam);
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }

        [ResponseType(typeof(Hesaplarim))]
        [HttpPost]
        [Route("create")]
        public IHttpActionResult PostHesaplarim(Hesaplarim hesaplarim)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var datam = dal.CreateHesaps(hesaplarim);
                    return CreatedAtRoute("Default Api", new { id = hesaplarim.SQ_Id }, hesaplarim);
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }

        [ResponseType(typeof(Hesaplarim))]
        [HttpPut]
        [Route("update")]
        public IHttpActionResult PutHesaplarim(int id, Hesaplarim hesaplarim)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                else if (dal.IsThereAnyData(id) == false)
                {
                    return NotFound();
                }
                else
                {
                    var datam = dal.UpdateHesaps(id, hesaplarim);
                    return Ok(datam);
                }
            }
            catch (System.Exception)
            {
                return BadRequest();
            }

        }
         
        [HttpDelete]
        [Route("delete")]
        public IHttpActionResult DeleteHesaplarim(int id)
        {
            try { 
            if (dal.IsThereAnyData(id) == false)
            {
                return NotFound();
            }
            else
            {
                dal.DeleteHesaps(id);
                return Ok();
                }
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }
    }
}
