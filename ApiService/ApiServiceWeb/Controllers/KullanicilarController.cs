﻿using ApiServiceDAL;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ApiServiceWeb.Controllers
{
    [RoutePrefix("api/Kullanicilar")]
    public class KullanicilarController : ApiController
    {
        KullanicilarDAL dal = new KullanicilarDAL();

        [ResponseType(typeof(IEnumerable<Kullanicilar>))]
        [HttpGet]
        [Route("findall")]
        public IHttpActionResult GetUsers()
        {
            try
            {
                var datam = dal.GetAllUsers();
                return Ok(datam);
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }

        [ResponseType(typeof(Kullanicilar))]
        [HttpGet]
        [Route("find/{id}")]
        public IHttpActionResult GetUsersByID(int id)
        {
            try
            {
                var datam = dal.GetAllUsersById(id);
                if (datam == null)
                {
                    return NotFound();
                }

                return Ok(datam);
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }

        [ResponseType(typeof(Kullanicilar))]
        [HttpPost]
        [Route("create")]
        public IHttpActionResult PostUsers(Kullanicilar users)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var datam = dal.CreateUsers(users);

                    return Ok(datam);
                }
                return BadRequest(ModelState);
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }

        [ResponseType(typeof(Kullanicilar))]
        [HttpPut]
        [Route("update")]
        public IHttpActionResult PutUsers(int id, Kullanicilar users)
        {
            try
            {
                if (dal.IsThereAnyData(id) == false)
                {
                    return NotFound();
                }
                else if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                else
                {
                    var datam = dal.UpdateUsers(id, users);
                    return Ok(users);
                }
            }
            catch (System.Exception)
            {

                return BadRequest();
            }

        }
         
        [HttpDelete]
        [Route("delete")]
        public IHttpActionResult DeleteUser(int id)
        {
            try
            { 
            if (dal.IsThereAnyData(id) == false)
            {
                return NotFound();
            }
            else
            {
                dal.DeleteUsers(id);
                return Ok();
                }
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }
    }
}