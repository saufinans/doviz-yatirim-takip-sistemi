﻿using ApiServiceDAL;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ApiServiceWeb.Controllers
{
    [RoutePrefix("api/Yetkiler")]
    public class YetkilerController : ApiController
    {

        YetkilerDAL dal = new YetkilerDAL();

        [ResponseType(typeof(IEnumerable<Yetkiler>))]
        [HttpGet]
        [Route("findall")]
        public IHttpActionResult GetYetkiler()
        {
            try
            {
                var datam = dal.GetAllYetkiler();
                return Ok(datam);
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
           
        }

        [ResponseType(typeof(Yetkiler))]
        [HttpGet]
        [Route("find/{id}")]
        public IHttpActionResult GetYetkilerByID(int id)
        {
            try
            {
                var datam = dal.GetYetkilerById(id);
                if (datam == null)
                    return NotFound();
                return Ok(datam);
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
          
        }

        [ResponseType(typeof(Yetkiler))]
        [HttpPost]
        [Route("create")]
        public IHttpActionResult PostYetkiler(Yetkiler yetkiler)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var datam = dal.CreateYetkiler(yetkiler);

                    return CreatedAtRoute("DefaultApi", new { id = yetkiler.SQ_Id }, datam);
                }
                return BadRequest(ModelState);
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
           
        }

        [ResponseType(typeof(Yetkiler))]
        [HttpPut]
        [Route("update")]
        public IHttpActionResult PutYetkiler(int id, Yetkiler yetkiler)
        {
            try
            {
                if (dal.IsThereAnyData(id) == false)
                {
                    return NotFound();
                }
                else if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                else
                {
                    var datam = dal.UpdateYetkiler(id, yetkiler);
                    return Ok(datam);
                }

            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }
         
        [HttpDelete]
        [Route("delete")]
        public IHttpActionResult DeleteYetkiler(int id)
        {
            try
            {
                if (dal.IsThereAnyData(id) == false)
                {
                    return NotFound();
                }
                else
                {
                    dal.DeleteYetkiler(id);
                    return Ok();
                }
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }
    }
}
