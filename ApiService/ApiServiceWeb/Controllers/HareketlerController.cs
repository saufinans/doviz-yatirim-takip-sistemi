﻿using ApiServiceDAL;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace ApiServiceWeb.Controllers
{
    [RoutePrefix("api/Hareketler")]
    public class HareketlerController : ApiController
    {
        HareketlerDAL dal = new HareketlerDAL();

        [ResponseType(typeof(IEnumerable<Hareketler>))]
        [HttpGet]
        [Route("findall")]
        public IHttpActionResult GetAllHareketler()
        {
            try
            {
                var datam = dal.GetHareketlers();
                return Ok(datam);
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }

        [ResponseType(typeof(Hareketler))]
        [HttpGet]
        public IHttpActionResult GetHareketlerById(int id)
        {
            try
            {
                var datam = dal.GetHareketlerByID(id);
                if (datam == null)
                {
                    return NotFound();
                }

                return Ok(datam);
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }

        [ResponseType(typeof(Hareketler))]
        [HttpPost]
        [Route("find/{id}")]
        public IHttpActionResult PostHareketler(Hareketler hareketler)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var datam = dal.CreateHareketler(hareketler);
                    return CreatedAtRoute("Default Api", new { id = hareketler.SQ_HareketId }, hareketler);
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }

        [ResponseType(typeof(Hareketler))]
        [HttpPut]
        [Route("update")]
        public IHttpActionResult PutHareketler(int id, Hareketler hareketler)
        {
            try { 
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else if (dal.IsThereAnyData(id) == false)
            {
                return NotFound();
            }
            else
            {
                var datam = dal.UpdateHareketler(id, hareketler);
                return Ok(datam);
                }
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }
         
        [HttpDelete]
        [Route("delete")]
        public IHttpActionResult DeleteHareketler(int id)
        {
            try { 
            if (dal.IsThereAnyData(id) == false)
            {
                return NotFound();
            }
            else
            {
                dal.DeleteHareketler(id);
                return Ok();
                }
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }
    }
}
