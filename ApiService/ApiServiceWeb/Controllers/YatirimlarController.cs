﻿using ApiServiceDAL;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;


namespace ApiServiceWeb.Controllers
{
    [RoutePrefix("api/Yatirimlar")]
    public class YatirimlarController : ApiController
    {
        YatirimlarDAL dal = new YatirimlarDAL();

        [ResponseType(typeof(IEnumerable<Yatirimlar>))]
        [HttpGet]
        [Route("findall")]
        public IHttpActionResult GetYatirimlar()
        {
            try
            {
                var datam = dal.GetYatirimlar();
                return Ok(datam);
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }

        [ResponseType(typeof(Yatirimlar))]
        [HttpGet]
        [Route("find/{id}")]
        public IHttpActionResult GetYatirimlarByID(int id)
        {
            try
            {
                var datam = dal.GetYatirimlarByID(id);
                if (datam == null)
                    return NotFound();
                return Ok(datam);
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }

        [ResponseType(typeof(Yatirimlar))]
        [HttpPost]
        [Route("create")]
        public IHttpActionResult PostYatirimlar(Yatirimlar yatirimlar)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var datam = dal.CreateYatirimlars(yatirimlar);

                    return CreatedAtRoute("DefaultApi", new { id = yatirimlar.SQ_Id }, datam);
                }
                return BadRequest(ModelState);
            }
            catch (System.Exception)
            {

                return BadRequest();
            }
        }
        [ResponseType(typeof(Yatirimlar))]
        [HttpPut]
        [Route("update")]
        public IHttpActionResult PutYatirimlar(int id, Yatirimlar yatirimlar)
        {
            try
            {

                if (dal.IsThereAnyData(id) == false)
                {
                    return NotFound();
                }
                else if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                else
                {
                    var datam = dal.UpdateYatirimlar(id, yatirimlar);
                    return Ok(datam);
                }
            }
            catch (System.Exception)
            {

                return BadRequest();
            }

        }
         
        [HttpDelete]
        [Route("delete")]
        public IHttpActionResult DeleteYatirimlar(int id)
        {
            try
            {

                if (dal.IsThereAnyData(id) == false)
                {
                    return NotFound();
                }
                else
                {
                    dal.DeleteYatirimlar(id);
                    return Ok();
                }
            }
            catch (System.Exception)
            {
                return BadRequest();
            }
        }
    }
}