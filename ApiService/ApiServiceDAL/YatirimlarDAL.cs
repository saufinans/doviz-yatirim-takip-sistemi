﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiServiceDAL
{
   public class YatirimlarDAL
    {
        FinansEntities db = new FinansEntities();

        public YatirimlarDAL()
        {
            db.Configuration.ProxyCreationEnabled = false;
        }

        public IEnumerable<Yatirimlar> GetYatirimlar()
        {
            return db.Yatirimlars;
        }

        public Yatirimlar GetYatirimlarByID(int id)
        {
            return db.Yatirimlars.Find(id);
        }

        public Yatirimlar CreateYatirimlars(Yatirimlar yatirimlar)
        {
            db.Yatirimlars.Add(yatirimlar);
            db.SaveChanges();
            return yatirimlar;
        }

        public Yatirimlar UpdateYatirimlar(int id, Yatirimlar yatirimlar)
        {
            db.Entry(yatirimlar).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return yatirimlar;
        }

        public void DeleteYatirimlar(int id)
        {
            db.Yatirimlars.Remove(db.Yatirimlars.Find(id));
            db.SaveChanges();
        }

        public bool IsThereAnyData(int id)
        {
            return db.Yatirimlars.Any(x => x.SQ_Id == id);
        }

    }
}
