﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiServiceDAL
{
    public class HesapTipDAL
    {
        FinansEntities db = new FinansEntities();

        public HesapTipDAL()
        {
            db.Configuration.ProxyCreationEnabled = false;
        }

        public IEnumerable<HesapTipleri> GetHesapTips()
        {
            return db.HesapTipleris;
        }

        public HesapTipleri GetHesapTipsByID(int id)
        {
            return db.HesapTipleris.Find(id);
        }

        public HesapTipleri CreateHesapTips(HesapTipleri hesapTipleri)
        {
            db.HesapTipleris.Add(hesapTipleri);
            db.SaveChanges();
            return hesapTipleri;
        }

        public HesapTipleri UpdateHesapTipleris(int id,HesapTipleri hesapTipleri)
        {
            db.Entry(hesapTipleri).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return hesapTipleri;
        }

        public void DeleteHesapTipleris(int id)
        {
            db.HesapTipleris.Remove(db.HesapTipleris.Find(id));
            db.SaveChanges();
        }

        public bool IsThereAnyData(int id)
        {
            return db.HesapTipleris.Any(x => x.SQ_Id == id);
        }

    }
}
