//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ApiServiceDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Hareketler
    {
        public int SQ_HareketId { get; set; }
        public string Durum { get; set; }
        public System.DateTime HareketTarihi { get; set; }
        public int HesapId { get; set; }
        public double Miktar { get; set; }
        public double HareketOncesiBakiye { get; set; }
        public double HareketSonrasiBakiye { get; set; }
    
        public virtual Hesaplarim Hesaplarim { get; set; }
    }
}
