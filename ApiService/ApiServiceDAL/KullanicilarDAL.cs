﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiServiceDAL
{
    public class KullanicilarDAL
    {
        FinansEntities db = new FinansEntities();

        public KullanicilarDAL()
        {
            db.Configuration.ProxyCreationEnabled = false;
        }

        public IEnumerable<Kullanicilar> GetAllUsers()
        {
            return db.Kullanicilars;
        }

        public Kullanicilar GetAllUsersById(int id)
        {
            return db.Kullanicilars.Find(id);
        }

        public Kullanicilar CreateUsers(Kullanicilar users)
        {
            db.Kullanicilars.Add(users);
            db.SaveChanges();
            return users;
        }

        public Kullanicilar UpdateUsers(int id,Kullanicilar users)
        {
            db.Entry(users).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return users;
        }

        public void DeleteUsers(int id)
        {
            db.Kullanicilars.Remove(db.Kullanicilars.Find(id));
            db.SaveChanges();
        }

        public bool IsThereAnyData(int id)
        {
            return db.Kullanicilars.Any(x => x.SQ_Id == id);
        }
    }
}
