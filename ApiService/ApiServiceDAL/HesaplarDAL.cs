﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiServiceDAL
{
   public class HesaplarDAL
    {
        FinansEntities db = new FinansEntities();

        public HesaplarDAL()
        {
            db.Configuration.ProxyCreationEnabled = false;
        }

        public IEnumerable<Hesaplarim> GetAllHesaplar()
        {
            return db.Hesaplarims;
        }

        public Hesaplarim GetAllHesapsById(int id)
        {
            return db.Hesaplarims.Find(id);
        }

        public Hesaplarim CreateHesaps(Hesaplarim hesaps)
        {
            db.Hesaplarims.Add(hesaps);
            db.SaveChanges();
            return hesaps;
        }

        public Hesaplarim UpdateHesaps(int id, Hesaplarim hesaps)
        {
            db.Entry(hesaps).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return hesaps;
        }

        public void DeleteHesaps(int id)
        {
            db.Hesaplarims.Remove(db.Hesaplarims.Find(id));
            db.SaveChanges();
        }

        public bool IsThereAnyData(int id)
        {
            return db.Hesaplarims.Any(x => x.SQ_Id == id);
        }
    }
}
