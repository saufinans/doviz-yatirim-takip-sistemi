﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiServiceDAL
{
    public class AlarmlarDAL
    {
        FinansEntities db = new FinansEntities();

        public AlarmlarDAL()
        {
            db.Configuration.ProxyCreationEnabled = false;
        }

        public IEnumerable<Alarmlar> GetAllAlarms()
        {
            return db.Alarmlars;
        }

        public Alarmlar GetAllAlarmsById(int id)
        {
            return db.Alarmlars.Find(id);
        }

        public Alarmlar CreateAlarms(Alarmlar alarms)
        {
            db.Alarmlars.Add(alarms);
            db.SaveChanges();
            return alarms;
        }

        public Alarmlar UpdateAlarms(int id, Alarmlar alarms)
        {
            db.Entry(alarms).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return alarms;
        }

        public void DeleteAlarms(int id)
        {
            db.Alarmlars.Remove(db.Alarmlars.Find(id));
            db.SaveChanges();
        }

        public bool IsThereAnyData(int id)
        {
            return db.Alarmlars.Any(x => x.SQ_Id == id);
        }
    }
}
