//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ApiServiceDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Hesaplarim
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Hesaplarim()
        {
            this.Alarmlars = new HashSet<Alarmlar>();
            this.Hareketlers = new HashSet<Hareketler>();
            this.Yatirimlars = new HashSet<Yatirimlar>();
        }
    
        public int SQ_Id { get; set; }
        public int KullaniciId { get; set; }
        public string HesapAdi { get; set; }
        public int HesapTipId { get; set; }
        public double HesapBakiye { get; set; }
        public System.DateTime AcilisTarihi { get; set; }
        public Nullable<System.DateTime> KapanisTarihi { get; set; }
        public bool HesapDurum { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Alarmlar> Alarmlars { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Hareketler> Hareketlers { get; set; }
        public virtual HesapTipleri HesapTipleri { get; set; }
        public virtual Kullanicilar Kullanicilar { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Yatirimlar> Yatirimlars { get; set; }
    }
}
