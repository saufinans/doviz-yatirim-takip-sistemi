﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiServiceDAL
{
    public class YetkilerDAL
    {
        FinansEntities db = new FinansEntities();

        public YetkilerDAL()
        {
            db.Configuration.ProxyCreationEnabled = false;
        }

        public IEnumerable<Yetkiler> GetAllYetkiler()
        {
            return db.Yetkilers;
        }

        public Yetkiler GetYetkilerById(int id)
        {
            return db.Yetkilers.Find(id);
        }

        public Yetkiler CreateYetkiler(Yetkiler yetkiler)
        {
            var datam = db.Yetkilers.Add(yetkiler);
            db.SaveChanges();
            return datam;
        }

        public Yetkiler UpdateYetkiler(int id, Yetkiler yetkiler)
        {
            db.Entry(yetkiler).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return yetkiler;

        }

        public void DeleteYetkiler(int id)
        {
            db.Yetkilers.Remove(db.Yetkilers.Find(id));
            db.SaveChanges();
        }

        public bool IsThereAnyData(int id)
        {
            return db.Yetkilers.Any(x => x.SQ_Id == id);
        }
    }
}
