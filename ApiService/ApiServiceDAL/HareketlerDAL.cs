﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiServiceDAL
{
    public class HareketlerDAL
    {
        FinansEntities db = new FinansEntities();

        public HareketlerDAL()
        {
            db.Configuration.ProxyCreationEnabled = false;
        }

        public IEnumerable<Hareketler> GetHareketlers()
        {
            return db.Hareketlers;
        }

        public Hareketler GetHareketlerByID(int id)
        {
            return db.Hareketlers.Find(id);
        }

        public Hareketler CreateHareketler(Hareketler hareket)
        {
            db.Hareketlers.Add(hareket);
            db.SaveChanges();
            return hareket;
        }

        public Hareketler UpdateHareketler(int id,Hareketler hareket)
        {
            db.Entry(hareket).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return hareket;
        }

        public void DeleteHareketler(int id)
        {
            db.Hareketlers.Remove(db.Hareketlers.Find(id));
            db.SaveChanges();
        }

        public bool IsThereAnyData(int id)
        {
            return db.Hareketlers.Any(x => x.HesapId == id);
        }

    }
}
